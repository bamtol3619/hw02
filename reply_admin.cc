//2015004057 김범수
#include <iostream>
#include <string>
#include <stdio.h>
#include <math.h>

using namespace std;

const int NUM_OF_CHAT = 200;

int stoi(string str){	//string을 int로 바꾸어주는 함수
	int val=0;
	for(int i=0;i<str.length();++i)	val+=pow(10,i)*((int)(str[str.length()-i-1])-48);
	return val;
}

int getChatCount(string *_chatList){
	int i;
	for(i=0; i<NUM_OF_CHAT; ++i){
		string s = _chatList[i];
		if(s.empty() == true)	break;
	}
	return i;
}

void printChat(string *_chatList){
	int count = getChatCount(_chatList);
	for(int i=0; i<count; ++i)	cout << i << " " << _chatList[i] << endl;
}

bool addChat(string *_chatList, string _chat); // returns true when adding chat is succeeded

bool removeChat(string *_chatList, int _index, int tries); // returns true when removing chat is succeeded

int main(void){
	string* chats = new string[NUM_OF_CHAT];

	addChat(chats, "Hello, Reply Administrator!");
	addChat(chats, "I will be a good programmer.");
	addChat(chats, "This class is awesome.");
	addChat(chats, "Professor Lim is wise.");
	addChat(chats, "Two TAs are kind and helpful.");
	addChat(chats, "I think male TA looks cool.");

	while(true){
		string command;
		getline(cin, command);
		int tries=1,index;
		bool remove_is_okay=false;

		if(command=="#quit")	break;
		else if(command.substr(0,8)=="#remove "){
			command.replace(0,8,"");
			if(command.find("-")>command.length() && command.find(",")>command.length())	//'-'이나 ','가 안 쓰인 remove명령
				index=stoi(command);
			else if(command.find("-")<command.length() && command.find(",")>command.length()){	//'-'만 쓰인 remove명령
				index=stoi(command.substr(0,command.find("-")));
				tries=stoi(command.substr(command.find("-")+1,command.length()-command.find("-")-1))-index+1;
			}
			else if(command.find("-")>command.length() && command.find(",")<command.length()){	//','만 쓰인 remove명령
				int i=0;
				remove_is_okay=true;
				while(command.find(",")<command.length()){
					index=stoi(command.substr(0,command.find(",")))-i;
					removeChat(chats,index,tries);
					command.replace(0,command.find(",")+1,"");
					if(index< getChatCount(chats))	++i;
				}
				index=stoi(command)-i;
				removeChat(chats,index,tries);
			}
		if(remove_is_okay)	printChat(chats);
		else if(removeChat(chats,index,tries))	printChat(chats);
		}
		else if(addChat(chats, command))	printChat(chats);
	}
	delete[] chats;
	return 0;
}

bool addChat(string *_chatList, string _chat){
	if(_chat.substr(0,1)!="#"){
		int count = getChatCount(_chatList);
		if(count < NUM_OF_CHAT){
			_chatList[count]=_chat;
			return true;
		}
		else
			return false;
	}
	return false;
}

bool removeChat(string *_chatList, int _index,int tries){
	int count = getChatCount(_chatList);
	if(_index >= count || tries < 1)	return false;
	else{
		for(int i=0;i<tries;++i){
			count = getChatCount(_chatList);
			for(int j=0;j<count-_index;++j)	_chatList[_index+j].assign(_chatList[_index+j+1]);
		}
		return true;
	}
}