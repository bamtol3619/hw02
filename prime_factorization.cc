//2015004057 김범수
#include <iostream>
#include <stdio.h>

using namespace std;

int primeFactorization(unsigned int _number);

int main(int argc, char** argv){
	if(argc < 1) return -1;
	unsigned int number;
	float input;
	if(sscanf(argv[1], "%f", &input)==1){
		if(input==(int)input && input>0){
			number=input;	//자연수를 입력해야만 프로그램이 실행되도록 한다.
			primeFactorization(number);
		}
	}
	return 0;
}

int primeFactorization(unsigned int _number){
	int prime=2,factor=0,i;
	if(_number==1){
		cout << "1^1" << endl;
		return 0;
	}
	for(i=1;_number!=1;++i){	//더 이상 나눌 수 없을때까지 실행.
		while(_number%prime==0){
			++factor;
			_number/=prime;
		}
		if(factor!=0){	//소수를 찾아 나누어 떨어진다면 출력.
			if(_number!=1)	cout << prime << "^" << factor << " " << "x" << " ";
			else	cout << prime << "^" << factor << endl;
		}
		prime=2*i+1;
		factor=0;
	}
	return 0;
}