//2015004057 김범수
#include <iostream>


using namespace std;

int main(){
	int x,y,row=0,col=0,i,j,k,number=0,pt[1000][2];	//pt는 점들의 모임(최대 1000개)
	while(1){
		cin >> x;
		cin >> y;
		if(x<0 || y<0)	break;
		row=((row>y+1) ? row:y+1);
		col=((col>x+1) ? col:x+1);	//row와 col은 나타내어야 할 좌표평면의 크기를 갱신
		pt[number][0]=x;
		pt[number][1]=y;
		++number;
		for(i=0;i<row;++i){
			for(j=0;j<col;++j){	//한번에 하나씩 점을 찍는데
				for(k=0;k<number;++k){
					if(pt[k][0]==j && pt[k][1]==i){	//점들의 모임에 해당되면 별을 찍는다
						cout << "*";
						break;
					}
					else if(k==number-1)	cout << ".";	//점들의 모임에 해당되는 곳이 아니었다면 점을 찍는다
				}
			}
			cout << endl;
		}
	}
	return 0;
}