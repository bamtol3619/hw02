//2015004057 김범수
#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

string RadixNotation(unsigned int number, unsigned int radix);

int main(int argc, char** argv){
	if (argc<2)	return -1;
	unsigned int radix;
	sscanf(argv[1], "%u", &radix);
	if (radix<2 || radix>36)	return -1;
	for (int i=2;i<argc;++i){
		unsigned int number;
		sscanf(argv[i], "%u", &number);
		cout << RadixNotation(number, radix) << endl;
	}
	return 0;
}

string RadixNotation(unsigned int number, unsigned int radix){
	string result;
	int r;
	while(number){
		r=number%radix;
		number/=radix;
		if(r>9)	result=char(87+r)+result;	//a~z까지의 아스키코드 이용
		else	result=char(48+r)+result;	//0~9까지의 아스키코드 이용
	}
	return result;
}
